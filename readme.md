This module provides integration between your website and your
<a href="https://cleancatalog.com">Clean Catalog</a> course catalog
or handbook site.

## Setup
1. First, you'll need to obtain your API username and API key. You can do so by
   contacting Clean Catalog support at
   <a href="mailto:support@cleancatalog.com">support@cleancatalog.com</a>.
2. After obtaining your API username and API key, enable this module,
   then go to the configuration page at
   `/admin/config/clean_catalog/cleancatalogconfig`
3. On that page, enter your API username and key, and enter the URL for your
   site, e.g. `https://catalog.yourschool.edu`
4. Current functionality is for displaying degrees in blocks. To add a degree
   block, go to `/admin/structure/block` and select "place block" in the region
   you'd like to place your block in.
5. Select "Degree Block," and then select the degree you'd like to display.


We're always open to new ideas, so if there's a feature you'd like that you
don't currently see in the module, feel free to contact
<a href="mailto:support@cleancatalog.com">support@cleancatalog.com</a> or
your account rep.
