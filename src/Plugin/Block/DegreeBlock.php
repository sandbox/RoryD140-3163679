<?php

namespace Drupal\clean_catalog\Plugin\Block;

use Drupal\clean_catalog\CcApiConnector;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DegreeBlock' block to render a degree from Clean Catalog site.
 *
 * @Block(
 *  id = "degree_block",
 *  admin_label = @Translation("Degree block"),
 * )
 */
class DegreeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Clean Catalog API Connector.
   *
   * @var \Drupal\clean_catalog\CcApiConnector
   */
  protected $ccApiConnector;

  /**
   * Construct function.
   *
   * @inheritdoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CcApiConnector $ccApiConnector) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->CcApiConnector = $ccApiConnector;
  }

  /**
   * Create function.
   *
   * @inheritdoc
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cc_api_connector.default')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get list of degrees.
    $degrees = $this->CcApiConnector->getAllDegrees();

    $options = [];
    $options['default'] = t('None');

    foreach ($degrees as $nid => $value) {
      $options[$nid] = $value;
    }

    $form['degree_certificate_to_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Degree/Certificate to Display'),
      '#options' => $options,
      '#default_value' => 'default',
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['degree_certificate_to_display'] = $form_state->getValue('degree_certificate_to_display');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = $this->getDegreeMarkup();
    // Ensure all objects are arrays.
    $markup = json_decode(json_encode($markup), TRUE);

    $build = [];
    $build['#theme'] = 'degree_block';
    $markup = $this->addMarkupTags($markup);
    $build['#content'] = $markup;

    return $build;
  }

  /**
   * Get HTML markup for rendered degree.
   *
   * @return mixed
   *   HTML markup string
   */
  private function getDegreeMarkup() {
    $nid = $this->configuration['degree_certificate_to_display'];
    return $this->CcApiConnector->getRenderedNode($nid);
  }

  /**
   * Add appropriate markup tags.
   *
   * @param array $array
   *   The original data.
   *
   * @return mixed
   */
  private function addMarkupTags($array) {
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $array[$key] = $this->addMarkupTags($value);
      }
      else {
        $array[$key] = ['#markup' => $value];
      }
    }
    return $array;
  }

}
