<?php

namespace Drupal\clean_catalog\Plugin\Block;

use Drupal\clean_catalog\CcApiConnector;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'CoursesBlock' block.
 *
 * @Block(
 *  id = "courses_block",
 *  admin_label = @Translation("Courses block"),
 * )
 */
class CoursesBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\clean_catalog\CcApiConnectorInterface definition.
   *
   * @var \Drupal\clean_catalog\CcApiConnector
   */
  protected $ccApiConnector;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('cc_api_connector.default')
    );
  }

  /**
   * Construct function.
   *
   * @inheritdoc
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CcApiConnector $ccApiConnector) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->ccApiConnector = $ccApiConnector;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get list of programs.
    $programs = $this->ccApiConnector->getAllPrograms();

    $options = [];
    $options['default'] = t('None');

    foreach ($programs as $tid => $name) {
      $options[$tid] = $name;
    }

    $form['courses_to_display'] = [
      '#type' => 'select',
      '#title' => $this->t('Courses to Display'),
      '#options' => $options,
      '#default_value' => $this->configuration['courses_to_display'],
      '#size' => 0,
      '#weight' => '0',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['courses_to_display'] = $form_state->getValue('courses_to_display');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $markup = $this->getCoursesMarkup();
    // Ensure all objects are arrays.
    $markup = json_decode(json_encode($markup), TRUE);

    $build = [];
    $build['#theme'] = 'courses_block';
    $markup = $this->addMarkupTags($markup);
    $build['#content'] = $markup;

    return $build;
  }

  /**
   * Get HTML markup for rendered degree.
   *
   * @return mixed
   *   HTML markup string
   */
  private function getCoursesMarkup() {
    $tid = $this->configuration['courses_to_display'];
    return $this->CcApiConnector->getCourses($tid);
  }

  /**
   * Add appropriate markup tags.
   *
   * @param array $array
   *   The original data array.
   *
   * @return mixed
   *   The data array with markup tags.
   */
  private function addMarkupTags(array $array) {
    foreach ($array as $key => $value) {
      if (is_array($value)) {
        $array[$key] = $this->addMarkupTags($value);
      }
      else {
        $array[$key] = ['#markup' => $value];
      }
    }
    return $array;
  }

}
