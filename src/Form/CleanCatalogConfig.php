<?php

namespace Drupal\clean_catalog\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration for Clean Catalog integration module.
 */
class CleanCatalogConfig extends ConfigFormBase {

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'clean_catalog.cleancatalogconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'clean_catalog_config';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('clean_catalog.cleancatalogconfig');

    $form['url_of_your_catalog_site'] = [
      '#type' => 'url',
      '#title' => $this->t('URL of your catalog site'),
      '#description' => $this->t('e.g. &quot;catalog.yourschool.edu&quot;'),
      '#default_value' => $config->get('url_of_your_catalog_site'),
    ];
    $form['api_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Username'),
      '#maxlength' => 64,
      '#size' => 64,
      '#default_value' => $config->get('api_username'),
    ];

    $form['authentication'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Authentication'),
    ];

    $key_exists = $this->moduleHandler->moduleExists('key');

    $requirenewkey = TRUE;
    if (!$key_exists && !empty($config->get('api_key'))) {
      $form['authentication']['secretkeynotice'] = [
        '#markup' => $this->t('You have saved a secret key. You may change the key by inputing a new one in the field directly below.'),
      ];
      $requirenewkey = FALSE;
    }

    if ($key_exists) {
      $form['authentication']['api_key'] = [
        '#type' => 'key_select',
        '#required' => TRUE,
        '#default_value' => $config->get('api_key'),
        '#title' => $this->t('API Key'),
        '#description' => $this->t('Your API key.'),
      ];
    }
    else {
      $form['authentication']['api_key'] = [
        '#type' => 'password',
        '#required' => $requirenewkey,
        '#title' => $this->t('api_key'),
        '#description' => $this->t('The secret key of your key pair. Your existing key is hidden. If you need to change this, provide a new key here. If you don&#039;t have one, contact support at support@cleancatalog.com'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    if ($this->moduleHandler->moduleExists('key')) {
      $key_name = $form_state->getValue('api_key');
      $this->config('clean_catalog.cleancatalogconfig')->set('api_key', $key_name);
    }
    else {
      if ($form_state->hasValue('api_key') && !empty($form_state->getValue('api_key'))) {
        $this->config('clean_catalog.cleancatalogconfig')
          ->set('api_key', $form_state->getValue('api_key'));
      }
    }

    $this->config('clean_catalog.cleancatalogconfig')
      ->set('url_of_your_catalog_site', $form_state->getValue('url_of_your_catalog_site'))
      ->set('api_username', $form_state->getValue('api_username'))
      ->set('api_key', $form_state->getValue('api_key'))
      ->save();
  }

}
