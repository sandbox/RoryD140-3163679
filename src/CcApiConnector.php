<?php

namespace Drupal\clean_catalog;

use GuzzleHttp\Exception\RequestException;

/**
 * Connect to Clean Catalog site API to retrieve data.
 */
class CcApiConnector {

  /**
   * The URL of the catalog site to connect to.
   *
   * @var string
   */
  protected $endpoint;

  /**
   * The API key to authenticate with.
   *
   * @var string
   */
  private $apiKey;

  /**
   * The API username to authenticate with.
   *
   * @var string
   */
  private $apiUser;


  /**
   * The Http Client.
   *
   * @var \http\Client
   */
  protected $client;

  /**
   * Constructs a new CcApiConnector object.
   */
  public function __construct() {

    // @todo dependency injection
    $config = \Drupal::config('clean_catalog.cleancatalogconfig');

    $this->apiKey = $config->get('api_key');
    $this->apiUser = $config->get('api_username');

    $this->endpoint = $config->get('url_of_your_catalog_site');

    $this->client = \Drupal::httpClient();

  }

  /**
   * Connect to Clean Catalog site API.
   *
   * @param string $url
   *   The path to connect to.
   *
   * @return mixed
   *   Value returned from Clean Catalog API.
   */
  private function connectToApi($url) {
    try {
      $connect_url = $this->endpoint . $url;
      $request = $this->client->get($connect_url, [
        'auth' => [$this->apiUser, $this->apiKey],
      ]);
      $file_contents = $request->getBody()->getContents();
    }
    catch (RequestException $e) {
      // An error happened.
      \Drupal::logger('clean_catalog')->error($e->getMessage());
    }

    return json_decode($file_contents);

  }

  /**
   * Get all degrees, optionally restrict by program.
   *
   * @return array
   *   Key value array where keys are node IDs and values are node titles.
   */
  public function getAllDegrees($program_tid = NULL) {

    // // Alternate URL if no value given. This is outside our cc_api module, just the default JSON export
    $url = '/cc_api/degrees/' . $program_tid . '?_format=json';
    if (!$program_tid) {
      $url = '/degrees/json?_format=json';
    }

    // Returns a json_encoded array with Node IDs as keys and titles as values.
    $degrees = $this->connectToApi($url);

    return $degrees;

  }

  /**
   * @param string $uuid
   * @return mixed
   */
  public function getDegreeWithEmbeddedInfo(string $uuid) {

    $url = '/jsonapi/node/degree/' . $uuid . '?include=field_degree_section_p.field_degree_section_courses';

    // Returns a json_encoded array with embedded paragraph info.
    $degree = $this->connectToApi($url);

    return $degree;
  }

  /**
   * @param string $uuid
   * @return mixed
   */
  public function getElectiveGroupWithEmbeddedInfo(string $uuid) {

    $url = '/jsonapi/node/elective_group/' . $uuid . '?include=field_elective_group_section.field_degree_section_courses';

    // Returns a json_encoded array with embedded paragraph info.
    $degree = $this->connectToApi($url);

    return $degree;
  }


  /**
   * Get all programs.
   *
   * @return array
   *   Key value array where keys are node IDs and values are node titles.
   */
  public function getAllPrograms() {

    $url = '/cc_api/all_programs?_format=json';

    // Returns a json_encoded array with Node IDs as keys and titles as values.
    $programs = $this->connectToApi($url);

    return json_decode($programs, TRUE);

  }

  /**
   * Get the rendered HTML for a node as it appears on your catalog site.
   *
   * @param int $nid
   *   The node ID to retrieve.
   *
   * @return mixed
   *   Returns string of HTML for rendered degree.
   */
  public function getRenderedNode($nid) {

    $url = '/cc_api/rendered/' . $nid . '?_format=json';
    $node = $this->connectToApi($url);

    return $node;

  }

  /**
   * Get array of information for an individual course.
   *
   * @param int $nid
   *   The node ID to retrieve.
   *
   * @return array
   *   Returns array of course information.
   */
  public function getCourse($nid) {

    $course = $this->connectToApi("/node/" . $nid . "?_format=json");

    $course_array = [];

    $course_array['item'] = $course->title[0]->value;
    $course_array['title'] = $course->field_item[0]->value;
    $course_array['credits'] = $course->field_credits[0]->value;

    return $course_array;
  }

  /**
   * Get courses for a program with the give TID.
   *
   * @param $tid
   *
   * @return mixed
   */
  public function getCourses($tid) {

    $courses = $this->connectToApi("/cc_api/courses/" . $tid . "?_format=json");
    return $courses;
  }

  /**
   * Gets JSON feed of all courses.
   */
  public function getAllCourses() {
    $courses = $this->connectToApi("/classes/json?_format=json");
    return $courses;
  }

  /**
   * Gets JSON feed of all courses.
   */
  public function getAllElectiveGroups() {
    $courses = $this->connectToApi("/elective_groups/json?_format=json");
    return $courses;
  }


  /**
   * @param int $tid
   *   Get individual Term.
   *   Requires Term Rest Resource to be enabled, get method
   */
  public function getTerm(int $tid) {
    $term = $this->connectToApi("/taxonomy/term/" . $tid . "?_format=json");
    return $term;
  }

}
